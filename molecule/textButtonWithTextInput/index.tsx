import { Color, Font, TextInput } from "@uikit/atom";
import { TextButtonType } from "./index.d";
import {
  BottomArea,
  ButtonContainer,
  CheckBox,
  CheckImg,
  TopArea,
} from "./index.style";
import checkImg from "@icon/appoint/check.png";

const TextButtonWithTextInput = ({
  title,
  onClick,
  isSelected,
  value,
  onChange,
  type,
  placeholder,
  height,
  width,
  multiLine,
}: TextButtonType) => {
  return (
    <ButtonContainer isSelected={isSelected} onClick={() => onClick()}>
      <TopArea>
        <CheckBox isSelected={isSelected}>
          {isSelected ? <CheckImg src={checkImg} /> : null}
        </CheckBox>
        <Font
          font={"regular"}
          size={18}
          color={isSelected ? Color.mainBlue : Color.black80}
        >
          {title}
        </Font>
      </TopArea>
      {isSelected ? (
        <BottomArea>
          <TextInput
            value={value}
            onChange={onChange}
            type={type}
            placeholder={placeholder}
            height={height}
            width={width}
            multiLine={multiLine}
          />
        </BottomArea>
      ) : null}
    </ButtonContainer>
  );
};

export default TextButtonWithTextInput;

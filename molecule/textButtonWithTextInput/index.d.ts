type TextButtonType = {
  title: string;
  isSelected: boolean;
  onClick: Function;
  value: string | number;
  onChange: Function;
  type: "text" | "password" | "number";
  placeholder?: string;
  height?: any;
  width?: any;
  multiLine: boolean;
};

type IsSelectedType = {
  isSelected: boolean;
};

export type { TextButtonType, IsSelectedType };

import { CheckBoxType } from "./index.d";
import { Container, Check } from "./index.style";
import checkBox_checked from "./icon/checkBox_checked.png";
import checkBox_unChecked from "./icon/checkBox_unChecked.png";
import { Color, Font } from "..";

const CheckBox = ({ title, onClick, isSelected }: CheckBoxType) => {
  return (
    <Container onClick={() => onClick()}>
      <Check src={isSelected ? checkBox_checked : checkBox_unChecked} />
      <Font
        font={isSelected ? "bold" : "regular"}
        size={16}
        color={Color.black100}
      >
        {title}
      </Font>
    </Container>
  );
};

export default CheckBox;

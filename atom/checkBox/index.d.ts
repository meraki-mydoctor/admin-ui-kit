type CheckBoxType = {
  onClick: Function;
  title: string;
  isSelected: boolean;
};

export type { CheckBoxType };

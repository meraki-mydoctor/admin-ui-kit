import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
`;

const Check = styled.img`
  width: 16px;
  height: 16px;
  margin-right: 10px;
`;

export { Container, Check };

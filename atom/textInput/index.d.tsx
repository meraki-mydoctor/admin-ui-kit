type TextInputType = {
  value: string | number;
  title?: string;
  onChange: Function;
  type: "text" | "password" | "number";
  placeholder?: string;
  height?: any;
  width?: any;
  onKeyPress?: Function;
  multiLine?: boolean;
  disabled?: boolean;
  fontSize?: any;
};

type TextInputContentType = {
  width: any;
  height: any;
  fontSize: any;
};

export type { TextInputType, TextInputContentType };

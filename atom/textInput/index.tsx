import styled from "styled-components";
import { Color, Font } from "@uikit/atom";
import { TextInputContentType, TextInputType } from "./index.d";

const TextInput = ({
  title,
  onChange,
  value,
  type,
  placeholder,
  onKeyPress,
  width,
  height,
  multiLine = false,
  disabled,
  fontSize
}: TextInputType) => {
  return (
    <TextInputContainer>
      {title && (
        <TextInputTitle>
          <Font font={"regular"} size={14} color={Color.black100}>
            {title}
          </Font>
        </TextInputTitle>
      )}
      {multiLine ? (
        <TextAreaComponent
          value={value}
          onChange={(e) => onChange(e)}
          onKeyPress={(e) => (onKeyPress ? onKeyPress(e) : {})}
          placeholder={placeholder}
          width={width}
          height={height}
          fontSize={fontSize}
        />
      ) : (
        <TextInputComponent
          onWheel={(e) => e.currentTarget.blur()}
          value={value}
          onChange={(e) => onChange(e)}
          onKeyPress={(e) => (onKeyPress ? onKeyPress(e) : {})}
          placeholder={placeholder}
          type={type}
          width={width}
          height={height}
          fontSize={fontSize}
          disabled={disabled}
        />
      )}
    </TextInputContainer>
  );
};

const TextInputContainer = styled.div``;
const TextInputTitle = styled.div`
  margin-bottom: 10px;
`;
const TextInputComponent = styled.input<TextInputContentType>`
  width: ${(props) => (props.width ? props.width : "calc(100% - 24px)")};
  height: ${(props) => (props.height ? props.height : 60)}px;
  padding-left: 20px;
  border: 1px solid ${Color.gray};
  border-radius: 4px;
  font-weight: 400;
  font-size: ${(props) => (props.fontSize || "18px")};
`;
const TextAreaComponent = styled.textarea<TextInputContentType>`
  width: ${(props) => (props.width ? props.width : "calc(100% - 24px)")};
  height: ${(props) => (props.height ? props.height : 60)}px;
  padding-left: 20px;
  border-width: 1px;
  border-color: ${Color.gray};
  font: 400;
  font-size: 18px;
  box-shadow: none;
  -moz-box-shadow: none;
  -webkit-box-shadow: none;
`;

export default TextInput;

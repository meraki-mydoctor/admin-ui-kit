import { Color } from "@uikit/atom";

export const modalCommonStyle = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: "40px",
    paddingBottom: "60px",
    width: "600px",
    backgroundColor: Color.white100,
    maxHeight: "80vh",
  },
  overlay: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
};

import { Color, Font } from "@uikit/atom";
import { TextButtonType } from "./index.d";
import { ButtonContainer, CheckBox, CheckImg } from "./index.style";
import checkImg from "@icon/appoint/check.png";

const TextButton = ({ title, onClick, isSelected }: TextButtonType) => {
  return (
    <ButtonContainer isSelected={isSelected} onClick={() => onClick()}>
      <CheckBox isSelected={isSelected}>
        {isSelected ? <CheckImg src={checkImg} /> : null}
      </CheckBox>
      <Font
        font={"regular"}
        size={18}
        color={isSelected ? Color.mainBlue : Color.black80}
      >
        {title}
      </Font>
    </ButtonContainer>
  );
};

export default TextButton;

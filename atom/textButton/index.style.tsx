import styled from "styled-components";
import { Color } from "..";
import { IsSelectedType } from "./index.d";

const ButtonContainer = styled.div<IsSelectedType>`
  display: flex;
  flex-direction: row;
  padding-top: 15px;
  padding-bottom: 15px;
  width: 100%;
  align-items: center;
  cursor: pointer;
  border: 1px solid
    ${(props) => (props.isSelected ? Color.mainBlue : Color.gray)};
`;

const CheckBox = styled.div<IsSelectedType>`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 18px;
  margin-right: 18px;
  border: 1px solid
    ${(props) => (props.isSelected ? Color.mainBlue : Color.gray)};
  width: 18px;
  height: 18px;
`;

const CheckImg = styled.img`
  width: 11px;
  height: 8.5px;
`;

export { ButtonContainer, CheckBox, CheckImg };

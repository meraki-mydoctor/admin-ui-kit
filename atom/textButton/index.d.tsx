type TextButtonType = {
  title: string;
  isSelected: boolean;
  onClick: Function;
};

type IsSelectedType = {
  isSelected: boolean;
};

export type { TextButtonType, IsSelectedType };

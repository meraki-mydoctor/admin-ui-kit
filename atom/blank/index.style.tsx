import styled from "styled-components";
import { BlankItemType } from "./index.d";
const HorizontalBlank = styled.div<BlankItemType>`
  width: ${(props) => props.size}px;
`;

const VerticalBlank = styled.div<BlankItemType>`
  height: ${(props) => props.size}px;
`;

export { HorizontalBlank, VerticalBlank };

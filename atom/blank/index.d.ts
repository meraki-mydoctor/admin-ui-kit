type BlankItemType = {
  size: number;
};

type BlankType = {
  size: number;
  appoint: "Horizontal" | "Vertical";
};

export type { BlankType, BlankItemType };

type CheckButtonType = {
  onClick: Function;
  title: string;
  isSelected: boolean;
};

export type { CheckButtonType };
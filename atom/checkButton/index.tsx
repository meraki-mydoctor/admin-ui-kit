import { CheckButtonType } from "./index.d";
import { Container, Check } from "./index.style";
import checkButton_checked from "./icon/checkButton_checked.png";
import checkButton_unChecked from "./icon/checkButton_unChecked.png";
import { Color, Font } from "..";

const CheckButton = ({ title, onClick, isSelected }: CheckButtonType) => {
  return (
    <Container onClick={() => onClick()}>
      <Check src={isSelected ? checkButton_checked : checkButton_unChecked} />
      <Font
        font={isSelected ? "bold" : "regular"}
        size={16}
        color={Color.black80}
      >
        {title}
      </Font>
    </Container>
  );
};

export default CheckButton;
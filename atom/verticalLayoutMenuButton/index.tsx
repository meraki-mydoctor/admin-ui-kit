import styled from "styled-components";
import { Color, Font } from "..";

interface VerticalLayoutMenuButtonInterface {
  title: Array<string> | string;
  disable: boolean;
  onClick?: Function;
}

const VerticalLayoutMenuButton = ({
  title,
  disable,
  onClick = () => {},
}: VerticalLayoutMenuButtonInterface) => {
  if (typeof title === "string") {
    return (
      <Button onClick={() => onClick()}>
        <Font
          font={"bold"}
          size={18}
          color={disable ? Color.black100 : Color.black80}
        >
          {title}
        </Font>
      </Button>
    );
  } else {
    return (
      <Button onClick={() => onClick()}>
        <Font
          font={"bold"}
          size={18}
          color={disable ? Color.black100 : Color.black80}
        >
          {title[0]}
        </Font>
        <Space />
        <Font
          font={"bold"}
          size={18}
          color={disable ? Color.mainBlue : Color.sub_blueSky}
        >
          {title[1]}
        </Font>
      </Button>
    );
  }
};

const Button = styled.div`
  cursor: pointer;
  display: flex;
  padding: 12px;
  padding-left: 40px;
  background-color: ${Color.white100};
  border-radius: 4px;
`;
const Space = styled.div`
  width: 5px;
`;

export default VerticalLayoutMenuButton;

type ColorType = {
  [index: string]: string;
  black100: string;
  black90: string;
  black80: string;
  black60: string;
  gray: string;
  white100: string;
  white90: string;
  white80: string;
  white40: string;
  bg: string;
  blueGray: string;
  mainBlue: string;
  blue40: string;
  blue20: string;
  red_alert: string;
  sub_pink: string;
  sub_orange: string;
  sub_yellow: string;
  sub_green: string;
  sub_blueGreen: string;
  sub_blueSky: string;
  sub_purple: string;
  kakao_yellow: string;
};

export type { ColorType };

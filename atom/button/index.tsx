import styled from "styled-components";
import { ButtonType } from "./index.d";

const Button = ({
  visible,
  backgroundColor,
  borderColor,
  height,
  title,
  onClick,
  disableColor,
}: ButtonType) => {
  let buttonColor = visible ? backgroundColor : disableColor;

  return (
    <ButtonContainer
      height={height}
      backgroundColor={buttonColor}
      borderColor={borderColor}
      visible={visible}
      onClick={() => (visible ? onClick() : {})}
    >
      {title}
    </ButtonContainer>
  );
};

const ButtonContainer = styled.div<{
  height: string;
  backgroundColor: string | undefined;
  borderColor?: string;
  visible: boolean;
}>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  border: ${(props) =>
    props.borderColor ? `1px solid ${props.borderColor}` : "0px"};
  height: ${(props) => props.height};
  background-color: ${(props) => props.backgroundColor};
  cursor: ${(props) => (props.visible ? "pointer" : "default")};
`;

export default Button;

import { ReactFragment } from "react";

type ButtonType = {
  visible: boolean;
  title: ReactFragment;
  onClick: Function;
  height: "40px" | "50px" | "60px";
  backgroundColor: string;
  borderColor?: string;
  disableColor?: string;
};

export type { ButtonType };

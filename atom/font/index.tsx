import { Color } from "..";
import { FontType } from "./index.d";
import { FontStyle } from "./index.styled";

const Font = ({ children, font, color = Color.black100, size }: FontType) => {
  let weight = "500";
  switch (font) {
    case "extraBold":
      weight = "800";
      break;
    case "regular":
      weight = "400";
      break;
    case "bold":
      weight = "700";
      break;
    case "semiBold":
      weight = "600";
      break;
  }

  return (
    <FontStyle size={size} color={color} weight={weight}>
      {children}
    </FontStyle>
  );
};

export default Font;

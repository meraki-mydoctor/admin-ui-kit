import styled from "styled-components";
import { FontStyleType } from "./index.d";

const FontStyle = styled.span<FontStyleType>`
  font-size: ${(props) => props.size}px;
  font-weight: ${(props) => props.weight};
  color: ${(props) => (props.color ? props.color : "#000000")};
`;

export { FontStyle };

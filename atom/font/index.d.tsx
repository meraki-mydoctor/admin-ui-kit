type FontType = {
  children?: string | any;
  font: "regular" | "semiBold" | "bold" | "extraBold";
  size: number;
  color?: string;
};

type FontStyleType = {
  size: number;
  weight: string;
  color?: string;
};

export type { FontStyleType, FontType };

import styled from "styled-components";

interface InputInterface {
  value: number | string;
  onChange: Function;
  width?: number;
  onKeyPress?: Function;
}

const Input = ({
  value,
  onChange,
  width = 100,
  onKeyPress = () => null,
}: InputInterface) => {
  return (
    <InputContainer
      onChange={(e) => onChange(e)}
      value={value}
      width={width}
      onKeyPress={(e) => onKeyPress(e)}
    />
  );
};

const InputContainer = styled.input<{ width: number }>`
  width: ${(props) => props.width}px;
  height: 60px;
  font-size: 20px;
  font-weight: 700;
  background-color: #fff;
`;

export default Input;

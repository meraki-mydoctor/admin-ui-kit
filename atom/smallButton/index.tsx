import styled from "styled-components";

interface SmallButtonInterface {
  title: string;
  onClick: Function;
}

const SmallButton = ({ title, onClick }: SmallButtonInterface) => {
  return (
    <ButtonContainer onClick={() => onClick()}>
      <Title>{title}</Title>
    </ButtonContainer>
  );
};

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50px;
  height: 30px;
  background-color: pink;
  border-radius: 4px;
  cursor: pointer;
`;
const Title = styled.span`
  font-size: 15px;
  font-weight: 500;
`;

export default SmallButton;

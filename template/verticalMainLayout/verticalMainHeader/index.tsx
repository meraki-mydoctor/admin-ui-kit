import { Color } from "@uikit/atom";
import React from "react";
import styled from "styled-components";

const VerticalMainHeader = ({ children }: any) => {
  return <Container>{children}</Container>;
};

const Container = styled.div`
  display: flex;
  position: fixed;
  flex-direction: column;
  border: 1px solid ${Color.gray};
  width: 220px;
  height: calc(100% - 56px);
  margin-top: 57px;
`;

export default VerticalMainHeader;

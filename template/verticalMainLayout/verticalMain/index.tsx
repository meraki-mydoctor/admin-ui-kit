import { Color } from "@uikit/atom";
import React from "react";
import styled from "styled-components";

const VerticalMain = ({ children }: any) => {
  return <Container>{children}</Container>;
};

const Container = styled.div`
  display: flex;
  flex: 1;
  margin-left: 222px;
  margin-top: 58px;
  background-color: ${Color.bg};
`;

export default VerticalMain;

import React from "react";
import styled from "styled-components";

const VerticalMainLayout = ({ children }: any) => {
  return <Container>{children}</Container>;
};

const Container = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  flex-direction: column;
`;

export default VerticalMainLayout;
